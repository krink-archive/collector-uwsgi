#!/bin/sh
#
# collector - this script starts and stops the collector
#
# chkconfig:   - 85 15
# description: collector
# processname: collector
# config:      /var/run/collector

# Source function library.
. /etc/rc.d/init.d/functions

PATH=/sbin:/bin:/usr/sbin:/usr/bin
PROG=/usr/bin/collector
OWNER=daemon
NAME=daemon
DESC="collector"
DAEMON_OPTS="--daemon"

lockfile=/var/lock/subsys/collector

start () {
  echo -n "Starting $NAME $DESC: "
  /bin/mkdir -p /var/run/collector
  /bin/chown daemon:daemon /var/run/collector

  cd /var/run/collector && daemon --user=daemon $PROG $DAEMON_OPTS
  retval=$?
  echo
  [ $retval -eq 0 ] && touch $lockfile
  return $retval
}

stop () {
  echo -n "Stopping $NAME $DESC: "
  killproc $PROG -INT
  retval=$?
  echo
  [ $retval -eq 0 ] && rm -f $lockfile
  return $retval
}

reload () {
  echo "Reloading $NAME"
  killproc $PROG -HUP
  RETVAL=$?
  echo
}

restart () {
    stop
    start
}

rh_status () {
  status $PROG
}

rh_status_q() {
  rh_status >/dev/null 2>&1
}

case "$1" in
  start)
    rh_status_q && exit 0
    $1
    ;;
  stop)
    rh_status_q || exit 0
    $1
    ;;
  restart)
    $1
    ;;
  reload)
    rh_status_q || exit 7
    $1
    ;;
  status)
    rh_status
    ;;
  condrestart|try-restart)
    rh_status_q || exit 0
    restart
    ;;
  *)
    echo "Usage: $0 {start|stop|restart|condrestart|try-restart|reload|status}" >&2
    exit 2
    ;;
esac
exit 0
