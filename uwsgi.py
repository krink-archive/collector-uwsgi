
__vesion__ = 'collector.uwsgi.server.0005b'

import os
import re
import json

rrd_store  = '/data/rrd'
auth_file  = '/data/rrd/hosts.allow.txt'

writetxt = False
response = False
debug    = False

def application(environ, start_response):

    headers = [('content-type', 'application/json')]

    try:
        import rrdtool
    except ImportError as e:
        start_response('200 OK', headers)
        if debug: print str(e) + ': yum install rrdtool-python'
        if response:
            return [str(e), ': yum install rrdtool-python',]
        else:
            return ['',]

    output_data = ''

    if 'CONTENT_LENGTH' in environ and environ['CONTENT_LENGTH'] != '':
        length = int(environ.get('CONTENT_LENGTH', '0'))
        #input = environ['wsgi.input'].read(length).decode()
        input = environ['wsgi.input'].read(length)
        json_data = json.loads(input)
    else:
        length = 0
        input = 'no input'
        start_response('200 OK', headers)
        if debug: print (input)
        if response:
            return [str(input),]
        else:
            return ['',]

    #####################################################################
    try:
        id = json_data['id']
    except KeyError as e:
        id = 'Empty'
        start_response('200 OK', headers)
        if debug: print str(e) + id
        if response:
            return [str(e), id]
        else:
            return ['',]

    #####################################################################
    try:
        with open(auth_file,'r') as f:
            authlist = f.read().splitlines()
    except IOError as e:
        start_response('200 OK', headers)
        if debug: print str(e)
        if response:
            return [str(e),]
        else:
            return ['',]


    if debug:
        print str(authlist)
        print 'id type: ' + str(type(id))

    #####################################################################
    # match file/id ##############################################
    match = None
    pattern = re.compile(u'^\\b' + id + '\\b$', re.UNICODE)
    for line in authlist:
        line = unicode(line, 'utf-8')
        #print 'line: ' + line + ' ' + str(type(line))
        if pattern.match(line):
            match = 'yes'
            if debug: print 'YES, matched!!! ' + id + ' ' + str(type(id))
            break

    if not match:
        start_response('200 OK', headers)
        if debug: print 'No match ' + id
        if response:
            return ['No match ' + str(id),]
        else:
            return ['',]

    ################################################################

    try:
        name = json_data['name']
    except KeyError as e:
        name = 'Empty'
        start_response('200 OK', headers)
        if debug: print str(e) + name
        if response:
            return [str(e), name]
        else:
            return ['',]

    if not os.path.isdir(rrd_store + '/' + name):
        print 'missing: ' + rrd_store + '/' + name
        try:
            os.mkdir(rrd_store + '/' + name, 0755)
        except OSError as e:
            start_response('200 OK', headers)
            if debug: print str(e)
            if response:
                return [str(e),]
            else:
                return ['',]


    ################################################################

    #print str(json_data)

    try:
        rrddict = json_data['db']
    except KeyError as e:
        rrddcit = 'Empty'
        start_response('200 OK', headers)
        if debug: print str(e) + rrddcit
        if response:
            return [str(e), rrddcit]
        else:
            return ['',]

    #print 'DBs ' + str(json_data['db'])
    #print 'DBs ' + str(rrddict)
    #print '--------------'

    for db in rrddict:
        #print str(db)
        rrd = db['rrd']
        val = db['val']
        rrdfile =  rrd_store + '/' + name + '/' + rrd + '.rrd'
        if debug: print 'rrdfile ' + rrdfile  +  ' val is ' + val


        if rrd == 'mpstat' and not os.path.isfile(rrdfile):
            mpstatRRD(rrdfile)
            continue

        if rrd == 'mem' and not os.path.isfile(rrdfile):
            memRRD(rrdfile)
            continue

        if rrd == 'iostat' and not os.path.isfile(rrdfile):
            iostatRRD(rrdfile)
            continue

        if rrd == 'ps' and not os.path.isfile(rrdfile):
            psRRD(rrdfile)
            continue

        if rrd == 'root' and not os.path.isfile(rrdfile):
            rootRRD(rrdfile)
            continue

        if rrd == 'swap' and not os.path.isfile(rrdfile):
            swapRRD(rrdfile)
            continue

        if rrd == 'uptime' and not os.path.isfile(rrdfile):
            uptimeRRD(rrdfile)
            continue

        if rrd == 'sbm' and not os.path.isfile(rrdfile):
            sbmRRD(rrdfile)
            continue

        if rrd == 'mysql' and not os.path.isfile(rrdfile):
            mysqlRRD(rrdfile)
            continue

        try:
            rrdtool.update(str(rrdfile), str(val))
        except rrdtool.error, e:
            if debug: print 'Error ' + str(e)

    #
    ##################################################
    #

    if debug: print 'The End'
    start_response('200 OK', headers)
    return ['',]

    # END MAIN
################################################################################################
################################################################################################
################################################################################################
################################################################################################

def psRRD(rrdfile=None):
#ps_rrdupdate = str(epochtime) + ':' + number_of_procs
#ps_rrdupdate += ':' + number_of_defunct
    cmdline = 'rrdtool create ' + rrdfile
    cmdline += ' --start 0 --step 300 '
    cmdline += ' DS:procs:GAUGE:600:U:U '
    cmdline += ' DS:defunct:GAUGE:600:U:U '
    cmdline += ' RRA:AVERAGE:0.5:1:360 '
    cmdline += ' RRA:AVERAGE:0.5:12:1008 '
    cmdline += ' RRA:AVERAGE:0.5:288:2016 '
    if debug: print "cmdline: " + cmdline
    os.system(cmdline)
    return True

#def diskRRD(rrdfile=None):
def iostatRRD(rrdfile=None):
#iostat
#['dm-4', '342.73', '1670.65', '2661.46', '24692779871', '39337324729']
#Device:            tps   Blk_read/s   Blk_wrtn/s   Blk_read   Blk_wrtn
    cmdline = 'rrdtool create ' + rrdfile
    cmdline += ' --start 0 --step 300 '
    cmdline += ' DS:tps:GAUGE:600:U:U '
    cmdline += ' DS:blk_reads:GAUGE:600:U:U '
    cmdline += ' DS:blk_wrtns:GAUGE:600:U:U '
    cmdline += ' DS:blk_read:GAUGE:600:U:U '
    cmdline += ' DS:blk_wrtn:GAUGE:600:U:U '
    cmdline += ' RRA:AVERAGE:0.5:1:360 '
    cmdline += ' RRA:AVERAGE:0.5:12:1008 '
    cmdline += ' RRA:AVERAGE:0.5:288:2016 '
    if debug: print "cmdline: " + cmdline
    os.system(cmdline)
    return True



def rootRRD(rrdfile=None):
#root_fs_size: 32125
#root_fs_used: 2566
#root_fs_avail: 27922
#root_fs_use: 9%
    #print "running rrdtool create"
    cmdline = 'rrdtool create ' + rrdfile
    cmdline += ' --start 0 --step 300 '
    cmdline += ' DS:size:GAUGE:600:U:U '
    cmdline += ' DS:used:GAUGE:600:U:U '
    cmdline += ' DS:avail:GAUGE:600:U:U '
    cmdline += ' DS:use:GAUGE:600:U:U '
    cmdline += ' RRA:AVERAGE:0.5:1:360 '
    cmdline += ' RRA:AVERAGE:0.5:12:1008 '
    cmdline += ' RRA:AVERAGE:0.5:288:2016 '
    if debug: print "cmdline: " + cmdline
    os.system(cmdline)
    return True


def mpstatRRD(rrdfile=None):
# mpstat
# 03:27:26 PM  CPU    %usr   %nice    %sys %iowait    %irq   %soft  %steal  %guest   %idle
# 03:27:26 PM  all    1.48    0.00    0.59    0.25    0.00    0.02    0.00    0.00   97.66
    #print "running rrdtool create"
    cmdline = 'rrdtool create ' + rrdfile
    cmdline += ' --start 0 --step 300 '
    cmdline += ' DS:usr:GAUGE:600:U:U '
    cmdline += ' DS:nice:GAUGE:600:U:U '
    cmdline += ' DS:sys:GAUGE:600:U:U '
    cmdline += ' DS:iowait:GAUGE:600:U:U '
    cmdline += ' DS:irq:GAUGE:600:U:U '
    cmdline += ' DS:soft:GAUGE:600:U:U '
    cmdline += ' DS:steal:GAUGE:600:U:U '
    cmdline += ' DS:guest:GAUGE:600:U:U '
    cmdline += ' DS:idle:GAUGE:600:U:U '
    cmdline += ' RRA:AVERAGE:0.5:1:360 '
    cmdline += ' RRA:AVERAGE:0.5:12:1008 '
    cmdline += ' RRA:AVERAGE:0.5:288:2016 '
    if debug: print "cmdline: " + cmdline
    os.system(cmdline)
    return True

# the round robin archive
#'RRA:AVERAGE:0.5:1:360'    Archive point is saved every 5min, archive is kept for 1day 6hour back.
#'RRA:AVERAGE:0.5:12:1008'  Archive point is saved every 1hour, archive is kept for 1month 11day back.
#'RRA:AVERAGE:0.5:288:2016' Archive point is saved every 1day, archive is kept for 5year 6month 8day back.

def memRRD(rrdfile=None):
# free -m
#             total       used       free     shared    buffers     cached
#Mem:          3819       3553        266          0        251       2977
    #print "running rrdtool create"
    cmdline = 'rrdtool create ' + rrdfile
    cmdline += ' --start 0 --step 300 '
    cmdline += ' DS:total:GAUGE:600:U:U '
    cmdline += ' DS:used:GAUGE:600:U:U '
    cmdline += ' DS:free:GAUGE:600:U:U '
    cmdline += ' DS:shared:GAUGE:600:U:U '
    cmdline += ' DS:buffers:GAUGE:600:U:U '
    cmdline += ' DS:cached:GAUGE:600:U:U '
    cmdline += ' RRA:AVERAGE:0.5:1:360 '
    cmdline += ' RRA:AVERAGE:0.5:12:1008 '
    cmdline += ' RRA:AVERAGE:0.5:288:2016 '
    if debug: print "cmdline: " + cmdline
    os.system(cmdline)
    return True

def swapRRD(rrdfile=None):
# free -m
#             total       used       free
#Swap:         5999          0       5999
    #print "running rrdtool create"
    cmdline = 'rrdtool create ' + rrdfile
    cmdline += ' --start 0 --step 300 '
    cmdline += ' DS:total:GAUGE:600:U:U '
    cmdline += ' DS:used:GAUGE:600:U:U '
    cmdline += ' DS:free:GAUGE:600:U:U '
    cmdline += ' RRA:AVERAGE:0.5:1:360 '
    cmdline += ' RRA:AVERAGE:0.5:12:1008 '
    cmdline += ' RRA:AVERAGE:0.5:288:2016 '
    if debug: print "cmdline: " + cmdline
    os.system(cmdline)
    return True

def uptimeRRD(rrdfile=None):
# uptime
#14:55  up 3 days,  3:20, 15 users, load averages: 1.67 1.58 1.63
#and remember no days...
#{ "rrd": "uptime", "val": "1469852403:1:5:0.82:0.77:0.74" }
    cmdline = 'rrdtool create ' + rrdfile
    cmdline += ' --start 0 --step 300 '
    cmdline += ' DS:days:GAUGE:600:U:U '
    cmdline += ' DS:users:GAUGE:600:U:U '
    cmdline += ' DS:one:GAUGE:600:U:U '
    cmdline += ' DS:five:GAUGE:600:U:U '
    cmdline += ' DS:fifteen:GAUGE:600:U:U '
    cmdline += ' RRA:AVERAGE:0.5:1:360 '
    cmdline += ' RRA:AVERAGE:0.5:12:1008 '
    cmdline += ' RRA:AVERAGE:0.5:288:2016 '
    if debug: print "cmdline: " + cmdline
    os.system(cmdline)
    return True

def sbmRRD(rrdfile=None):
    name = 'sbm'
    cmdline = 'rrdtool create ' + rrdfile
    cmdline += ' --start 0 --step 300 '
    cmdline += ' DS:%s:GAUGE:600:0:U ' % name
    cmdline += ' RRA:AVERAGE:0.5:1:360 '
    cmdline += ' RRA:AVERAGE:0.5:10:1008 '
    if debug: print "cmdline: " + cmdline
    os.system(cmdline)
    return True

#Note: A ds-name must be 1 to 19 characters [a-zA-Z0-9_]
def mysqlRRD(rrdfile=None):
    __name__ = 'mysql'
    #print "running rrdtool create"
    cmdline = 'rrdtool create ' + rrdfile
    cmdline += ' --start 0 --step 300 '
    cmdline += ' DS:AbortedClients:GAUGE:600:U:U '
    cmdline += ' DS:AbortedConnects:GAUGE:600:U:U '
    cmdline += ' DS:AccessDeniedErrors:GAUGE:600:U:U '
    cmdline += ' DS:BytesReceived:GAUGE:600:U:U '
    cmdline += ' DS:BytesSent:GAUGE:600:U:U '
    cmdline += ' DS:Connections:GAUGE:600:U:U '
    cmdline += ' DS:CreatedTMPFiles:GAUGE:600:U:U '
    cmdline += ' DS:InnoBufPoolPgsData:GAUGE:600:U:U '
    cmdline += ' DS:InnoBufPoolBytsData:GAUGE:600:U:U '
    cmdline += ' DS:InnoBufPoolBytsDrty:GAUGE:600:U:U '
    cmdline += ' DS:InnoBufPoolPgsFlshd:GAUGE:600:U:U '
    cmdline += ' DS:InnoBufPoolPgsFree:GAUGE:600:U:U '
    cmdline += ' DS:InnoBufPoolPgsTotal:GAUGE:600:U:U '
    cmdline += ' DS:InnoBufPoolReads:GAUGE:600:U:U '
    cmdline += ' DS:InnoDataPendFsyncs:GAUGE:600:U:U '
    cmdline += ' DS:InnoDataPendReads:GAUGE:600:U:U '
    cmdline += ' DS:InnoDataPendWrites:GAUGE:600:U:U '
    cmdline += ' DS:InnoDataReads:GAUGE:600:U:U '
    cmdline += ' DS:InnoDataWrites:GAUGE:600:U:U '
    cmdline += ' DS:InnoDblWrites:GAUGE:600:U:U '
    cmdline += ' DS:InnoRowLockCurrWait:GAUGE:600:U:U '
    cmdline += ' DS:InnoRowLockTime:GAUGE:600:U:U '
    cmdline += ' DS:InnoRowLockTimeAvg:GAUGE:600:U:U '
    cmdline += ' DS:InnoRowLockTimeMax:GAUGE:600:U:U '
    cmdline += ' DS:InnoNumOpenFiles:GAUGE:600:U:U '
    cmdline += ' DS:InnoRowLockWaits:GAUGE:600:U:U '
    cmdline += ' DS:InnoRowsRead:GAUGE:600:U:U '
    cmdline += ' DS:InnoRowsUpdated:GAUGE:600:U:U '
    cmdline += ' DS:InnoRowsDeleted:GAUGE:600:U:U '
    cmdline += ' DS:InnoRowsInserted:GAUGE:600:U:U '
    cmdline += ' DS:MaxUsedConnections:GAUGE:600:U:U '
    cmdline += ' DS:MemoryUsed:GAUGE:600:U:U '
    cmdline += ' DS:OpenFiles:GAUGE:600:U:U '
    cmdline += ' DS:OpenTables:GAUGE:600:U:U '
    cmdline += ' DS:OpenedFiles:GAUGE:600:U:U '
    cmdline += ' DS:Opene_tables:GAUGE:600:U:U '
    cmdline += ' DS:QcacheHits:GAUGE:600:U:U '
    cmdline += ' DS:Queries:GAUGE:600:U:U '
    cmdline += ' DS:Questions:GAUGE:600:U:U '
    cmdline += ' DS:SlaveConnections:GAUGE:600:U:U '
    cmdline += ' DS:SlavesConnected:GAUGE:600:U:U '
    cmdline += ' DS:SlowQueries:GAUGE:600:U:U '
    cmdline += ' DS:ThreadsConnected:GAUGE:600:U:U '
    cmdline += ' DS:ThreadsRunning:GAUGE:600:U:U '
    cmdline += ' DS:Uptime:GAUGE:600:U:U '
    cmdline += ' RRA:AVERAGE:0.5:1:360 '
    cmdline += ' RRA:AVERAGE:0.5:12:1008 '
    cmdline += ' RRA:AVERAGE:0.5:288:2016 '
    if debug: print "cmdline: " + cmdline
    os.system(cmdline)
    return True


#EOF
