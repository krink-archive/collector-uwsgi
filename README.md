
# nginx uwsgi emporor virtualenv

#manual install

# yum install -y nginx

#curl mirror/epel/USAePay-epel.repo >/etc/yum.repos.d/USAePay-epel.repo
cat <<-EOE >/etc/yum.repos.d/USAePay-epel.repo
file:/etc/yum.repos.d/USAePay-epel.repo
[usaepay-epel]
name=USAePay-$releasever
baseurl=http://mirror/epel/$releasever/$basearch/
gpgcheck=0
enabled = 1
EOE


# yum install -y uwsgi
# yum install -y uwsgi-logger-file
# yum install -y uwsgi-plugin-python
#### yum install -y uwsgi-plugin-rrdtool #rrdstats for uwsgi

# yum install -y rrdtool-python


#nginx
#file:/etc/nginx/conf.d/default.conf

	# python uwsgi
	location /collector {
	    include       /etc/nginx/uwsgi_params;
	    uwsgi_pass    unix:///run/uwsgi/collector.socket;
	}


cp ini/uwsgi.ini-emporer /etc/uwsgi.ini
cp ini/collector.ini-emporer /etc/uwsgi.d/collector.ini

chown uwsgi /run/uwsgi

######### we don't necissarily need rrdcached...
# yum install -y rrdtool-cached
#file:/etc/default/rrdcached
RUN_RRDCACHED=1
RRDCACHED_USER="rrdcached"
OPTS="-w 300 -z 300 -R -B -b /data/rrd"
PIDFILE="/var/run/rrdcached/rrdcached.pid"
SOCKFILE="/var/run/rrdcached/rrdcached.sock"
SOCKPERMS=0666

#nginx ssl
mkdir /etc/nginx/ssl

openssl req -x509 -nodes -days 3650 -newkey rsa:2048 -keyout /etc/nginx/ssl/nginx.key -out /etc/nginx/ssl/nginx.crt

#file:/etc/nginx/conf.d/default.conf
server {
    listen       80;

    listen 443 ssl;
    ssl_certificate /etc/nginx/ssl/nginx.crt;
    ssl_certificate_key /etc/nginx/ssl/nginx.key;

    server_name  localhost;



# mkdir -p /data/rrd
# chown rrdcached.rrdcached /data/rrd
# /etc/init.d/rrdcached start



#yum install -y python-virtualenv
virtualenv virtualenv

