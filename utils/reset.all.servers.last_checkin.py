#!/bin/env python

__version__ = '0.0.1'

dbconf  = '/app/etc/db.conf'

with open(dbconf) as conf:
  for line in conf:
    if line.startswith("define('monDbUser'"):
      monDbUser = line.split(',')[1].strip('\'').split('\'')[0]
    if line.startswith("define('monDbPassword'"):
      monDbPassword = line.split(',')[1].strip('\'').split('\'')[0]
    if line.startswith("define('monDbServer'"):
      monDbServer = line.split(',')[1].strip('\'').split('\'')[0]
    if line.startswith("define('monDbName'"):
      monDbName = line.split(',')[1].strip('\'').split('\'')[0]

config = {
  'user': monDbUser,
  'password': monDbPassword,
  'host': monDbServer,
  'database': monDbName,
  'raise_on_warnings': True,
}


import sys
try:
  import mysql.connector
except ImportError as e:
  print e
  print 'yum install mysql-connector-python'
  sys.exit(1)


try:
  cnx = mysql.connector.connect(**config)
except mysql.connector.Error as e:
  print e
  sys.exit(1)

cursor = cnx.cursor()
try:
  cursor.execute("update " + monDbName + ".servers set last_checkin = '0000-00-00 00:00:00';")
  cnx.commit()
except mysql.connector.Error as e:
  print e
  sys.exit(1)

cursor.close()
cnx.close()


sys.exit(0)
