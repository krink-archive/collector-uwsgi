#!/bin/env python

__version__ = '0.0.2'

dbconf  = '/app/etc/db.conf'

import sys
import subprocess

with open(dbconf) as conf:
  for line in conf:
    if line.startswith("define('monDbUser'"):
      monDbUser = line.split(',')[1].strip('\'').split('\'')[0]
    if line.startswith("define('monDbPassword'"):
      monDbPassword = line.split(',')[1].strip('\'').split('\'')[0]
    if line.startswith("define('monDbServer'"):
      monDbServer = line.split(',')[1].strip('\'').split('\'')[0]
    if line.startswith("define('monDbName'"):
      monDbName = line.split(',')[1].strip('\'').split('\'')[0]

sql = 'select pin from ' + monDbName +'.servers;';
cmdline = ["mysql", "-N", "-h", monDbServer, "-u", monDbUser, "-p%s" % monDbPassword, "-e", sql ]

p = subprocess.Popen(cmdline, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=False)
output, err = p.communicate()
exit_code = p.wait()
if (exit_code != 0):
  print 'Error: ' + str(err) + ' ' + str(output)
  sys.exit(1)

multilines = output.splitlines()

for line in multilines:
  print line

sys.exit(0)
