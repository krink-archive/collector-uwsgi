#!/bin/env python

__version__ = '0.0.2'

dbconf  = '/app/etc/db.conf'

with open(dbconf) as conf:
  for line in conf:
    if line.startswith("define('monDbUser'"):
      monDbUser = line.split(',')[1].strip('\'').split('\'')[0]
    if line.startswith("define('monDbPassword'"):
      monDbPassword = line.split(',')[1].strip('\'').split('\'')[0]
    if line.startswith("define('monDbServer'"):
      monDbServer = line.split(',')[1].strip('\'').split('\'')[0]
    if line.startswith("define('monDbName'"):
      monDbName = line.split(',')[1].strip('\'').split('\'')[0]

config = {
  'user': monDbUser,
  'password': monDbPassword,
  'host': monDbServer,
  'database': monDbName,
  'raise_on_warnings': True,
}


import sys
try:
  import mysql.connector
except ImportError as e:
  print e
  print 'yum install mysql-connector-python'
  sys.exit(1)


try:
  cnx = mysql.connector.connect(**config)
except mysql.connector.Error as e:
  print e
  sys.exit(1)

cursor = cnx.cursor()
try:
  #sql = "truncate monitor.server_jobs;"
  sql = "delete from monitor.server_jobs;"
  cursor.execute(sql)
  cnx.commit()
  #print("affected rows = {}".format(cursor.rowcount))
  print 'affected rows = ' + str(cursor.rowcount)
except mysql.connector.Error as e:
  print e
  sys.exit(1)

cursor.close()
cnx.close()


sys.exit(0)



#sql = "rename table monitor.server_jobs to monitor.server_jobs_old; create table monitor.server_jobs like monitor.server_jobs_old;drop table monitor.server_jobs_old;"
#1142 (42000): DROP, ALTER command denied to user 'monitorfe'@'monitor' for table 'server_jobs'

#sql = "truncate monitor.server_jobs;"
#1142 (42000): DROP command denied to user 'monitorfe'@'monitor' for table 'server_jobs'

