#!/usr/bin/env bash

conf="/app/etc/db.conf"
#define('monDbUser','user');
#define('monDbPassword','pass');
#define('monDbServer','127.0.0.1');
#define('monDbName','monitor');

monDbUser=$(grep ^define $conf | grep monDbUser | awk -F"," '{print $2}' | awk -F"'" '{print $2}')
monDbPassword=$(grep ^define $conf | grep monDbPassword | awk -F"," '{print $2}' | awk -F"'" '{print $2}')
monDbServer=$(grep ^define $conf | grep monDbServer | awk -F"," '{print $2}' | awk -F"'" '{print $2}')
monDbName=$(grep ^define $conf | grep monDbName | awk -F"," '{print $2}' | awk -F"'" '{print $2}')

mysql -u$monDbUser -p$monDbPassword -h $monDbServer -P 3306

