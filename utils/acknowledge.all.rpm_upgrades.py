#!/bin/env python

__version__ = '0.0.2'

dbconf  = '/app/etc/db.conf'

with open(dbconf) as conf:
  for line in conf:
    if line.startswith("define('monDbUser'"):
      monDbUser = line.split(',')[1].strip('\'').split('\'')[0]
    if line.startswith("define('monDbPassword'"):
      monDbPassword = line.split(',')[1].strip('\'').split('\'')[0]
    if line.startswith("define('monDbServer'"):
      monDbServer = line.split(',')[1].strip('\'').split('\'')[0]
    if line.startswith("define('monDbName'"):
      monDbName = line.split(',')[1].strip('\'').split('\'')[0]

config = {
  'user': monDbUser,
  'password': monDbPassword,
  'host': monDbServer,
  'database': monDbName,
  'raise_on_warnings': True,
}


import sys
try:
  import mysql.connector
except ImportError as e:
  print e
  print 'yum install mysql-connector-python'
  sys.exit(1)


try:
  cnx = mysql.connector.connect(**config)
except mysql.connector.Error as e:
  print e
  sys.exit(1)

cursor = cnx.cursor()
try:
  sql = "update monitor.server_events set status='acknowledged' where status='paged' and subject = 'RPM Upgrades';"
  cursor.execute(sql)
  cnx.commit()
  #Python 2.6.6 (r266:84292, Aug 18 2016, 15:13:37)
  #print("affected rows = {}".format(cursor.rowcount))
  #ValueError: zero length field name in format
  print 'affected rows = ' + str(cursor.rowcount)
except mysql.connector.Error as e:
  print e
  sys.exit(1)

cursor.close()
cnx.close()


sys.exit(0)
